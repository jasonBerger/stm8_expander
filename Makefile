# STM8 device (default is STM8 discovery board)

VARIANT=STM8S003K3
DEVICE_FLASH=$(shell echo '$(VARIANT)' | tr '[:upper:]' '[:lower:]')
DEVICE=$(shell echo '$(VARIANT)' | cut -c 1-8)


TARGETNAME = stm8_expander

# trap handling requires SDCC >=v3.4.3
SKIP_TRAPS = 0

# set output folder and target name
OUTPUT_DIR = ./build
DOC_DIR = ./doc
TARGET     = $(OUTPUT_DIR)/$(TARGETNAME).elf
DOC = $(DOC_DIR)/_build 

# set project folder and files (all *.c)
PRJ_ROOT    = .
PRJ_SRC_DIR = $(PRJ_ROOT)/src
PRJ_INC_DIR = $(PRJ_ROOT)/src
PRJ_LIB_DIR = $(PRJ_ROOT)/lib
# all project sources included by default
PRJ_SOURCE  = $(notdir $(wildcard $(PRJ_SRC_DIR)/*.c))
PRJ_OBJECTS := $(addprefix $(OUTPUT_DIR)/, $(PRJ_SOURCE:.c=.rel))

#MRT Module path 
MRT_MODS = $(PRJ_ROOT)/MrT/Modules

# set SPL paths
# set SPL paths
SPL_ROOT    = /opt/STM8S_StdPeriph_Lib
SPL_SRC_DIR = $(SPL_ROOT)/Libraries/STM8S_StdPeriph_Driver/src
SPL_INC_DIR = $(SPL_ROOT)/Libraries/STM8S_StdPeriph_Driver/inc
SPL_SOURCE  = stm8s_gpio.c stm8s_i2c.c stm8s_clk.c stm8s_rst.c stm8s_exti.c
SPL_OBJECTS := $(addprefix $(OUTPUT_DIR)/, $(SPL_SOURCE:.c=.rel))

OBJLIB = $(PRJ_LIB_DIR)/stm8s003.lib
OBJLIBNDB = $(PRJ_LIB_DIR)/stm8s003nodebug.lib


# set compiler path & parameters 
CC      = sdcc
AR 	= sdar
# ifeq ($(NODEBUG), 1)
#     CFLAGS =-mstm8  -l$(OBJLIBNDB) --out-fmt-ihx -DUSE_STDPERIPH_DRIVER -D$(VARIANT) --all-callee-saves --verbose --stack-auto --fverbose-asm --no-peep
#     TARGET = $(OUTPUT_DIR)/$(TARGETNAME).ihx
# else
	CFLAGS =-mstm8 --debug --out-fmt-elf -DUSE_STDPERIPH_DRIVER -D$(VARIANT) --all-callee-saves --verbose --stack-auto --fverbose-asm --no-peep
    TARGET = $(OUTPUT_DIR)/$(TARGETNAME).elf
# endif

# collect all include folders
INCLUDE = -I$(PRJ_INC_DIR) -I$(SPL_INC_DIR)

# collect all source directories
VPATH=$(PRJ_SRC_DIR):$(SPL_SRC_DIR)

.PHONY: clean

all:  $(TARGET)

doc: $(DOC)

$(DOC): registers
	make -C $(DOC_DIR) docx

version:
	mrt-version src/version.h --patch auto

registers:
	mrt-device -s -i device.yml -o src -d doc 

gdbserver:
	openocd -s $(OPENOCD_SCRIPTS) -f interface/stlink-dap.cfg -f target/stm8s003.cfg -c "init" -c "reset halt"

debug: $(TARGET) 
	stm8-gdb $(TARGET)

$(OUTPUT_DIR)/%.rel: %.c
	mkdir -p $(@D)
	$(CC)  $(CFLAGS) -D$(DEVICE) $(INCLUDE) -DSKIP_TRAPS=$(SKIP_TRAPS) -c $? -o $@

$(TARGET): $(SPL_OBJECTS) $(PRJ_OBJECTS)
	$(CC)  $(CFLAGS) -o $(TARGET) $^
	

flash: $(TARGET)
	stm8flash -c stlinkv2 -p $(DEVICE_FLASH) -s flash -w $(TARGET)

clean:
	rm -rf $(OUTPUT_DIR)/*
	rm -rf $(DOC_DIR)/_build*

