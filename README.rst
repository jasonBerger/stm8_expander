STM8_Expander
=============

stm8_expander is a firmware build that runs on the STM8S003x3 MCU to turn it into an I2C GPIO expander. 



=================                ======================================================================
**Development Environment**
-------------------------------------------------------------------------------------------------------
=================                ======================================================================
**Language**                     C/C++
**IDE**                          VS Code / None       
**OS**                           Linux
**Dev Container**                `uprev/stm8 <https://hub.docker.com/repository/docker/uprev/stm8>`_
=================                ======================================================================


Building 
--------



The firmware can be built using the `uprev/stm8 <https://hub.docker.com/repository/docker/uprev/stm8>`_ docker container. Opening the folder in VS Code will allow you to use the 'Reopen in Container' option to mount the project in the development environment. 


Once in the devcontainer, run 'make' to build the firmware 

.. code:: bash 

    make


.. note:: To set up a host machine with the proper build tools, reference the Dockerfile for `uprev/stm8 <https://hub.docker.com/repository/docker/uprev/stm8>`_ to see which tools are needed. 

Flashing 
--------

.. code:: bash

    make flash 


.. note:: 
    
    Flashing/Debug is only available in the container with a Linux host. To share the USB devices with the container, uncomment the following lines in `devcontainer.json`:

    "mounts": ["type=bind,source=/dev/bus/usb,target=/dev/bus/usb"],
	"runArgs": ["--privileged"],


STM8 Detailed Dev information 
-----------------------------

https://github.com/TG9541/stm8ef/wiki/STM8S-Programming#stm8flash


Debugging
---------

To debug first start a gdbserver 

.. code:: bash 

    make gdbserver

Then select the Debug extension in VS code and run the 'STM8-gdb' configuration


Register Definitions
--------------------

Registers for the device are generated using the `mrt-device <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-device.html>`_ tool. The registers are defined in the `device.yml` file. 

To update the source: 

.. code:: bash 

    make registers


This generates the c code in the following files: 

- src/stm8_expander_slave.h
- src/stm8_expander_slave.c
- src/stm8_expander_regs.h


.. note:: Everything inside of /*gen-block-x-start*/ and /*gen-block-x-end*/ tags will be replaced when the register Definitions are updated 


Documentation 
-------------

The `./doc` directory contains a sphinx project for generating the user manual. All of the tools required are included in the devcontainer

.. code:: bash 

    make doc 


This will generate the user manual `/doc/_build/docx/STM8-Expander.docx`


