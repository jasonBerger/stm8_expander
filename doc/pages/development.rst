Development
-----------

The stm8-expander firmware is


=================                ======================================================================
**Development Environment**
-------------------------------------------------------------------------------------------------------
=================                ======================================================================
**Language**                     C/C++
**IDE**                          VS Code / None       
**OS**                           Linux
**Dev Container**                `uprev/stm8 <https://hub.docker.com/repository/docker/uprev/stm8>`_
=================                ======================================================================


Building 
~~~~~~~~

The firmware can be built using the `uprev/stm8 <https://hub.docker.com/repository/docker/uprev/stm8>`_ docker container. Opening the folder in VS Code will allow you to use the 'Reopen in Container' option to mount the project in the development environment. 


Once in the devcontainer, run 'make' to build the firmware 

.. code:: bash 

    make

Code Generation 
~~~~~~~~~~~~~~~

The firmware emulates registers like you would find on a dedicated IO expander. The structure of these registers is generated using `mrt-device <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-device.html>`_ . To make changes to the register structure edit the `device.yml` file and regenerate the source and documentation: 

.. code:: bash 
    mrt-device -s -i device.yml -o src -d doc

.. note:: the '-s' flag tells the tool to generate code for the slave device