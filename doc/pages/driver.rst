
Driver
------

There is a `MrT Module <https://bitbucket.org/uprev/device-stm8-expander>`_ which contains the driver for a controlling the IO expander over I2C.

The driver and register emulation code are generated with `mrt-device <https://mrt.readthedocs.io/en/latest/pages/mrtutils/mrt-device.html>`_. The registers are defined in the 'device.yml' file. 



To configure a GPIO 

.. code:: c 

    stm8_expander_t exp; 

    io_init_i2c(&exp, I2C1);                                          // Initialize expander on I2C1

    io_cfg_gpio_out(&exp, 0);                                         // Configure GPIO 0 to be an output 
    io_cfg_gpio_in(&exp, 1, IO_CFG_PP_ON, IO_CFG_IRQ_FALLING);        // Configure GPIO 1 to be an input with PUSH/Pull ON, and a falling trigger for IRQ

    io_set_gpio(&exp, 1, LOW);                                        // Sets GPIO output to LOW. Since it is configured as an input, this enables the internal pulldown resistor
    io_set_gpio(&exp, 0,HIGH);                                        // Sets GPIO 0 High

    io_cfg_irq(&exp, IO_IRQ_POLAR_LOW, 12)                            //Configure IRQ to pull GPIO 12 low when triggered
