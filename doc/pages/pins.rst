Pins
----

The stm8-expander firmware can be run on either the 20 or 32 pin variant of the STM8S003. 


.. figure:: ../assets/diagrams/lqfp32.dio.png
    :width: 500px
    :align: center

    LQFP32


.. figure:: ../assets/diagrams/qfn20.dio.png
    :width: 500px
    :align: center

    QFN20


.. figure:: ../assets/diagrams/tssop20.dio.png
    :width: 500px
    :align: center

    TSSOP20


.. csv-table:: LQFP32
   :header: "PIN", "Description", "Comments"
   :widths: 20,30,50

    "NRST", "Reset line. Must be pulled HIGH",""
    "VSS", "Ground",""   
    "VDD", "Digital Supply", "2V95 - 5V5"
    "VCAP", "Cap for internal 1V8 regulator", "u47"
    "SDA", "I2C Data", ""
    "SCL", "I2C Clock", ""
    "0-25", "GPIO", ""
    "ADCx", "Analog inputs", ""
    "PWMx", "PWM Outputs",""

.. note:: For more information on the parts see the `datasheet <https://www.st.com/resource/en/datasheet/stm8s003f3.pdf>`_