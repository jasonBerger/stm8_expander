Features 
--------

- Access to all GPIO 
- Configurable interrupt
    - Interrupts can be triggered by any GPIO/ADC input 
    - Adjustable threshold for ADC interrupts
    - Any GPIO can be used as output for interrupt 
- Access to 128 Bytes of internal EEPROM 


.. csv-table:: Feature Comparison
   :header: "Variant", "GPIO", "ADC", "PWM", "EEPROM"
   :widths: 20,10, 10, 10, 20

   "QFN20", 14, 5, 6, "128 Bytes"
   "TSSOP20", 14, 5, 6, "128 Bytes"
   "LQFP32", 25, 4, 7, "128 Bytes"
