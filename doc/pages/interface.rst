Interface 
=========


I2C Interface 
-------------

Registers can be read and written to over I2C. 

.. uml:: ../assets/uml/reg_read.puml

.. uml:: ../assets/uml/reg_read.puml


Register Map 
------------

.. include:: ../REGMAP.rst